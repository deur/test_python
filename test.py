def Increasing(number):
    # Increasing values / Valores incrementales
    boolean = True
    strNumber = str(number)
    for i in range(len(strNumber) - 1):
        if int(strNumber[i]) > int(strNumber[i+1]):
            boolean = False
            return boolean
    return boolean

def Decreasing(number):
    # Decreasing values / Valores decrementales
    boolean = True
    strNumber = str(number)
    for i in range(len(strNumber) - 1):
        if int(strNumber[i]) < int(strNumber[i+1]):
                boolean = False
                return boolean
    return boolean

def main():
    # Main function / Función principal
    n = 99
    zigzageante = 0
    percentageZig = 0.0
    while int(percentageZig) != 99:
        n+=1
        if Increasing(n) == False and Decreasing(n) == False:
                zigzageante +=1
        percentageZig = (zigzageante*100.0)/n
    print ('El numero menor zigzageante es:  ', n,', Con un porcentaje del: ', percentageZig)


if __name__ == '__main__':
    main()
